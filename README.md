# Sudoku #
###### [http://sudoku.blakepetersen.io](http://sudoku.blakepetersen.io)

This is a simple HTML5/JS/CSS Sudoku Webapp. 

The most difficult part of building a sudoku app would have to be finding a way to create a starting board that is randomized to create the final board. Essentialy, what you need to do is set up a board in such a fashion that each 3X3 set cycles the starting integer but maintains the same pattern. When each square is filled across all 9 sets, every column and row as well as each 3x3 set, contains each 1-9 integer in a unique arrangement. Once we have this, it's all downhill from here.

To randomize this staring board, you can take two random 1-9 integers and swap all 9 instances of each for the other. So if 4 and 8 were randomly selected, all the 4's would replace the 8's and vice versa. After such a swap, the board is made different while maintaining it's solved state. Because two numbers are randomly chosen and swapped as opposed to finding one random value and swapping interatively, we get a much more random outcome. For my app, I chose to interate this randomization 100x. 

The final step to creating a Sudoku board is to remove values that the user will fill in to recreate the completed, solved board. This offers an opportunity to introduce a difficulty variance in that you can make the board more challenging to solve by hiding additional nodes. For my app, I went with 'easy' (default) hiding 5 nodes per set, 'normal' hiding 6 nodes and, 'hard' hiding 7 nodes. I was considering offering four levels of difficulty (with an 'insane' mode) with the other three being each one node easier, may decide to go that route if I find the playing on easy is just not easy enough. 

Once the user is able to generate a board and add in values to empty blocks, you essentially have a working Sudoku app, all the rest is just bells and whistles. The bells and whistles I decided to add in before submission were resetting the board (based on stored difficulty setting), change difficulty (easy/normal/hard), show a single node's answer using the Hint button and to just solve the whole thing with the Solve function. It also will change green if the board is completed and no errors are present and will detect duplicates in a set and will flag the "flies in the ointment", keeping the solved change-to-green from occurring. I chose to keep this passive and not force the removal of a flagged entry too allow greater liberty on the user's part. However, should the issues with not being able to handle multiple errors per set/row/column, it may be easiest to alert the user of their mistake and force the correction/removal of the move at issue. 

If I had additional time, there's a lot I would have added, namely finish up the validation of the rows and columns. I would also add in a timer and "moves left" count down. I would also revisit some of the problems that came up during testing (see Known Issues), like spending more time on an actual iPhone device (I'm an android guy) to understand some of the native nuances (like the styles applied to the input buttons in iOS webkit/blink-based browser). For all potential features I'd like to integrate given the time, please see the Roadmap section below.


### Application Structure ###

You will find the code /public. The repo will be adjusted as soon as possible

public/
    css/
        style.css
    js/
        plugins.js
        scripts.js
    index.html

The application is developed using the fewest number of assets as possible to reduce the number of request needed to be made to render the single-page app. Libraries used (Font-awesome, jQuery) are referenced using CDN to allow for download concurrency and to potentially load from cache if the user's been to another site that's using the same public copy (highly likely with jQuery).

* _index.html_ - This page contains the markup which has a very simple structure. The head contains the dependencies like font-awesome and jQuery and the footer calls the business logic script assets. The Board is set up using 9 sets of 9 nodes, each node containing an input that will be manipulated by the script logic. There are two sets of buttons, the first that is persistently available to the user contains control buttons, the second is a modal window that opens to offer the user difficulty options.

* _style.css_ - The stylesheet contains the majority of the styles scoped for desktop and has a mobile breakpoint of 32em. With the help of some jQuery, the board built using a 1:1 aspect ratio, sized to the width of the screen (portrait mode-focused). When the mobile breakpoint is breached, the board width/height is sized to the exact width of the viewport less 1rem of padding. The buttons are also rendered as an off-canvas menu that slides up/down when the arrow is clicked. Special attention was made to ensure redundant properties didn't make it into the mobile-breakpoint media query. The styles are grouped according to their section and the element's spot in the markup, elements higher in the markup called first. 

* _plugins.js_ - This currently contains one plugin which is a cookie-handling jQuery plugin to help tame the storage cookies.

* _scripts.js_ - This is the bread and butter of the webapp. It's broken out into three general sections that could be called as independent assets, but, under ideal circumstances with minification turned on, we'd want to keep the total number of requests down and send it all in one fell swoop. These three sections are functions, buttons/actions, and page load init scripts. The buttons/actions and init scripts all leverage the functions and the functions are fairly limited in scope to allow for greater flexibility down the line should we want to repurpose logic in new features.


### Technical Considerations ###

* Pixels are avoided for the most part (some exceptions with shadows and such). Instead, REM is used to ensure a consistent sizing across browser/platforms.

* No raster images were used. Everything is fully scalable to support various device screen pixel densities. 

* All animations are made using CSS3 transforms/transitions by adding/removing classes for optimal performance (as opposed to using jQuery effects like slideUp(), etc);

* Data is stored using cookies, arrays are passed using JSON stringification. Not ideal, but it works and is fast. I wanted to keep the server resources miniscule, and that meant client-side storage of some capacity. 


### Current Features ###

* Difficulty Options (Easy, Normal, Hard)
* Hint: Solve a single random square for the user
* Solve: Solve the puzzle for the user
* Reset: Clear the board and start a new game
* Cookie-based session tracking/data storage


### Roadmap ###

* Better landscape-orientation support for mobile/tablet
* Passive Column Validation
* Passive Row Validation
* Timer
* Moves Remaining Countdown
* Move repository root to /public
* Integrate Firebase for serverless storage
* Save/Load Games
* Link devices using a code-pair schema avoiding storing data server-side
* Point system


### Known Issues ###

* Passive Set Validation reverts to valid after any valid change thereafter even if errors exist
* If user removes value, browser retains empty value attribute within input, being recognized as a filled space by the winning game checker