$(document).ready(function(){



	/* ==== FUNCTION FUNHOUSE ==== */


	/* -- Adjust board height to ensure 1:1 aspect ratio -- */

	function adjustViewHeight(){

		var viewHeight= $(window).height();
		$('.container').css('height', viewHeight);

	}


	
	/* -- Adjust board height to ensure 1:1 aspect ratio -- */

	function adjustBoardHeight(){

		var boardWidth = $('.board').width();
		$('.board').addClass('loaded').css('height', boardWidth);

	}



	/* -- Unique Game ID Generator -- */

	function createUniqueID(){

		function S4() {
		   return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
		}
		   
		return (S4()+S4()+S4());

	}





	/* --  Create completed 'simple' Sudoku by generating nine sets of nine nodes, cycling through each integer as the starting number -- */

	function clearBoard(){

		for (x = 0; x < 9; x++){

			for (y = 0; y < 9; y++){

				$('.set-' + (x + 1) + ' .node-' + (y + 1)).removeClass('revealed');
				$('.set-' + (x + 1) + ' .node-' + (y + 1) + ' input').val('').removeAttr('disabled');

			}

		}
			
		$('input[type=fly], input[type=ointment], input[type=solved]').removeAttr('type');


	}



	/* --  Create completed 'simple' Sudoku by generating nine sets of nine nodes, cycling through each integer as the starting number -- */

	function startingValues(gameData){

		for (x = 0; x < 9; x++){

			for (y = 0; y < 9; y++){

				/* -- 

					## Formula for outputting starting board array ##
					
						- First Set/Node Example where x = 0, y = 0
							
							- ((((0) + (Math.floor(0))) 0) + 1) = 1

						- Second Set/Node Example where x = 1, y = 1
							
							- (((1 + (3) + (0)) 0) + 1) = 5

						- Seventh Set/Node Example where x = 6, y = 6
							
							- (((6 + (18) + (2)) % 9) + 1)
							- ((26 % 9) + 1)
							- ((8) + 1) = 9

				-- */

				nodeVal = (((y + (x * 3) + (Math.floor(x / 3))) % 9) + 1);
				
				if (nodeVal > 9){

					nodeVal = nodeVal - 9;

				} 

				gameData[x + '.' + y] = nodeVal;

			}

		}

	}



	/* -- Randomize Board 100x -- */

	function randomizeValues(gameData){


		for (r = 0; r < 100; r++){


			/* -- If we find two numbers (1-9) and swap all instances, the board randomizes without breaking. Find two != intergers between 1 and 9 -- */

			var r1 = Math.ceil(Math.random() * 9);


			/* -- Redo duplicates -- */ 

			do {

				var r2 = Math.ceil(Math.random() * 9);

			} while ( r1 == r2 )


			/* -- Iterate through our lovely array, and swap the pairings -- */

			for (x = 0; x < 9; x++){

				for (y = 0; y < 9; y++){

					if (gameData[x + '.' + y] == r1){

						gameData[x + '.' + y] = r2;

					} else if (gameData[x + '.' + y] == r2){

						gameData[x + '.' + y] = r1;

					}

				}

			}


		}


	}



	/* -- Hide values based on difficulty. Easy = hide 5/set (default), Normal = hide 6/set, Hard = hide 7/set -- */

	function hideValues(gameData, difficulty){


 		if (difficulty == "normal") { 

			var hide = 6;

		} else if (difficulty == "hard") {

			var hide = 7;

		} else { 

			// Defaults to easy
			var hide = 5;

		} 


		var solutionData = {};
		
		
		/* -- Iterate through the sets -- */

		for (x = 0; x < 9; x++) {11


			/* -- Randomly X unique intergers from 1-9, based on difficulty -- */

			for (y = 0; y < hide; y++) {

				do {

					var h = Math.ceil(Math.random() * 9);

				} while ( gameData[x + '.' + (h - 1)] == 0 );

				solutionData[x + '.' + (h - 1)] = gameData[x + '.' + (h - 1)];

				gameData[x + '.' + (h - 1)] = 0;

			}


		}


		/* -- Store Solution Data -- */

		var solString = JSON.stringify(solutionData);
		$.cookie('sol', solString);

	}


	/* -- User's made a move, let's do some stuff about it -- */

	function moveDetected(el){ 


		var value = el.value;


		/* -- Numbers Only!!! But we don't want to annoy with alerts, so non-numerics get the heave-ho and they can try again -- */

	    value = value.replace(/[^1-9\.]/g, '');


	    /* -- Pull out grid coordinates from classes -- */

	    var x = $(el).closest('ul').attr('class');
	   	x = x.replace('set-','');

	    var y = $(el).parent('li').attr('class');
	    y = y.replace('node-','');


	    if (value == ''){


	    	/* -- Convert for stringification -- */

	    	value = 0;


	    	/* -- if no value, assume backspace, remove value with fallback -- */

	    	$('.set-' + (x + 1) + ' .node-' + (y + 1) + ' input').removeAttr('value').val('');


	    }


	    /* -- Move Data -- */

		var moveArray = [value, x, y];


	    /* -- Store as last move -- */

		var moveStr = JSON.stringify(moveArray);
		var lastMoveCookie = $.cookie('lm');


		/* -- Avoid duplicate changes to support detection of backspace on mobile keyboards -- */

		if (lastMoveCookie != moveStr) {

			$.cookie('lm', moveStr);
		    validateMove(value, x, y);

		}


	}


	function validateMove(value, x, y) {


		var set = x;
		var node = y;
		var errSet = '';


		/* -- Back to normal -- */

		if (value == 0) {
			value = '';
		}


		/* -- Check for Duplicates in Set -- */

		for (z = 0; z < 9; z++){

			currentNode = $('.set-' + set + ' .node-' + (z + 1) + ' input').attr('value');

			if (currentNode == value && value != 0) {

				errSet = 1;

				$('.set-' + set + ' .node-' + (z + 1) + ' input').attr('type','fly');

			} else {

				$('.set-' + set + ' .node-' + (z + 1) + ' input').removeAttr('type');

			}

		}


		/* -- Failed Validation -- */

		if (errSet == 1) {

			$('.set-' + set).attr('type','ointment');
			$('.set-' + set + ' .node-' + node + ' input').attr('type','fly');

		} else {

			$('.set-' + set + ', .set-' + set + ' .node-' + node + ' input').removeAttr('type');

		}


		/* -- Apply entry as input value -- */

		$('.set-' + set + ' .node-' + node + ' input').attr('value', value);


		/* -- Winning Move: Check if winning move by first counting the number of empty spaces, and if zero and doesn't have an error, we have a winner -- */

		var nonEmptyInputs = $("input[value]").length;

		if (nonEmptyInputs == 81 && errSet != 1) {

			$('.board').attr('type','solved');

		}
		

	}


	/* -- Renders the game board after data is properly shuffled and -- */
	function renderGame(gameData){

		for (x = 0; x < 9; x++){

			for (y = 0; y < 9; y++){

				if (gameData[x + '.' + y] != 0){

					$('.set-' + (x + 1) + ' .node-' + (y + 1)).addClass('revealed');
					$('.set-' + (x + 1) + ' .node-' + (y + 1) + ' input').val(gameData[x + '.' + y]).attr({'placeholder':gameData[x + '.' + y],'disabled':'disabled'});

				}

			}

		}

	}



	function createGame(difficulty){


		/* -- First things first, add Game ID cookie so we don't create games on page reloads when one exists-- */

		var gameID = createUniqueID();
		$.cookie('gameID', gameID);


		/* -- Check for first time players, if found, slap a unique id on em for life (unless they clear their cookies, that is... or the singularity turns out in our favor) -- */

		var newbCheck = $.cookie('uID');

		if (newbCheck == undefined) {
			var uID = createUniqueID();
			$.cookie('uID', uID, { expires: 99999 });
		}			


		/* -- Now, let's create an empty array that will store the solution data -- */

		var gameData = new Array();
		var solutionData = new Array();


		/* -- Clear Board-- */

		clearBoard();
		startingValues(gameData);
		randomizeValues(gameData);
		hideValues(gameData, difficulty);


		/* -- Pass game data over to our friends in the rendering department-- */

		renderGame(gameData);

	}



	function loadGame(gameID){

		console.log('Load Game');

	}



	function giveHint(){

		var solString = $.cookie('sol');
		var sol = JSON.parse(solString);
		var solKeys = Object.keys(sol);


		var nonEmptyInputs = $("input[value]").length;
		var luckyNumber = Math.ceil(Math.random() * nonEmptyInputs);


		var luckyNode = solKeys[luckyNumber];
		var luckyVal = sol[luckyNode];

		var luckyNodeArray = luckyNode.split(".");
		var set = parseInt(luckyNodeArray[0]);
		var node = parseInt(luckyNodeArray[1]);


		$('.set-' + (set + 1) + ' .node-' + (node + 1) + ' input').val(luckyVal);


		/* -- Delete revealed answer from solution array -- */

		delete sol[luckyNode];


		/* -- Update solution cookie -- */

		var solString = JSON.stringify(sol);
		$.cookie('sol', solString);

	}



	function solveBoard(){

		var solString = $.cookie('sol');
		var sol = JSON.parse(solString);


		for (x = 0; x < 9; x++){

			for (y = 0; y < 9; y++){

				if (sol[x + '.' + y] !== undefined){

					$('.set-' + (x + 1) + ' .node-' + (y + 1) + ' input').val(sol[x + '.' + y]);

				}

			}

		}

	}







	/* ==== Buttons / Actions ==== */


	/* -- Slide open Controls for Mobile -- */

	$('.open').click(function(){

		$('.controls').toggleClass('opened');

		var modal = $('.modal.open').length;

		if (modal == 1){

			$('.modal.open').removeClass('open');

		}

	});



	/* -- Open/Close Difficulty Modal -- */

	$('.difficulty, .close').click(function(){

		$('.modal').toggleClass('open');

	});


	/* -- User's making a change -- */

	$('.modal button').click(function(){

		var diffSet = $.cookie('diff');
		var diffReq = $(this).attr('class');


		/* -- If change is to a different difficulty, update cookie and create a new game. If the same difficulty is chosen, got back to original game. Also, change the button text -- */

		if (diffSet != diffReq) {

			$('.difficulty').html('<i class="fa fa-cog"></i>' + diffReq);
			$.cookie('diff', diffReq, { expires: 99999 });
			createGame(diffReq);

		} 


		/* -- Back to the board -- */

		$('.modal.open').removeClass('open');


	});


	/* -- Hint -- */

	$('.hint').click(function(){

		giveHint();

	});


	/* -- Solve -- */

	$('.solve').click(function(){

		solveBoard();

	});


	/* -- Reset -- */

	$('.reset').click(function(){

		var diff = $.cookie('diff');
		createGame(diff);

	});



	/* -- Move Made -- */

	$('input').on('change', function (){ 

		moveDetected(this);

	});


	$('input').keyup(function (){ 

		moveDetected(this);

	});






	/* ==== Fire Scripts on Page Load ==== */


	/* -- Make sure our widths and our heights are all on the same page, even if the viewport changes -- */

	adjustViewHeight();
	adjustBoardHeight();

	window.addEventListener("resize", function() {
	
		adjustViewHeight();
		adjustBoardHeight();

	}, false);


	/* -- Check if gameID already exists -- */

	var gameID = '';
	gameID = $.cookie('gameID');


	if (gameID == '') {


		/* -- If Game Exists, Load Game -- */

		loadGame(gameID);


	} else {


		/* -- Check for difficulty cookie, if none then apply default of 'easy' to cookie, set til 2288 -- */

		var diff = $.cookie('diff');

		if (diff == undefined) {

			$.cookie('diff', 'easy', { expires: 99999 });

		} else {

			$('.difficulty').html('<i class="fa fa-cog"></i>' + diff);

		}



		/* -- If Game Does Not Exist, Create Game -- */

		createGame(diff);


	}

	/* -- Instantiate FastClick on the doc body -- */

	new FastClick(document.body);



});